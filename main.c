/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet :  3                                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Chiffrement de messages                                                                *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 :  Rouillon Ambre                                                            *
*                                                                             *
*  Nom-pr�nom2 :  Vignolles Martin                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :  main.c                                                        *
*                                                                             *
******************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"


void main()
{
    printf("entrer le mot a coder :");
    char ACoder[100];
    scanf("%s",ACoder);
    int cle;
    printf("\nEntrez la cle :\n");
    scanf("%d", &cle);
    if(!verificationAlpa(ACoder)) {
        printf("\nUn ou plusieurs caracteres sont incorrect");
    } else {
        int i = 0;
        while(i < 1) {
            printf("appuyer sur 1 pour coder et sur 2 pour decoder: ");
            scanf("%d",&i);
        }

        if(i == 1) {
            retirerAccent(ACoder);
            cesar(&ACoder, cle);
            printf("%s", ACoder);
        } else {
            DecCesar(&ACoder, cle);
            printf("%s", ACoder);
        }


    }
}


char* cesar(char * str, int cle) {
  int i = 0;
  while (str[i]) {
    if (str[i] >= 'A' && str[i]<= 'Z') {
        char c = str[i] - 'A';
        c += cle;
        c = c%26;
        str[i] = c + 'A';
    } else if(str[i] >= 'a' && str[i] <= 'z') {
        char c = str[i] - 'a';
        c += cle;
        c = c%26;
        str[i] = c + 'a';
    }
    i++;
  }
  return str;
}


char* DecCesar(char * str, int cle) {
  int i = 0;
  while (str[i]) {
    if (str[i] >= 'A' && str[i]<= 'Z') {
        char c = str[i] - 'A';
        c -= cle;
        c = c%26;
        str[i] = c + 'A';
    } else if(str[i] >= 'a' && str[i] <= 'z') {
        char c = str[i] - 'a';
        c -= cle;
        c = c%26;
        str[i] = c + 'a';
    }
    i++;
  }
  return str;
}


int verificationAlpa (char str[]) {
    int i = 0;
    int c = 0;
    int vf = 0;
    while (str[i] != '\0') {
        if (isalpha(str[i])) {
           c++;
        }
        i++;
    }
    if ( c == strlen(str)) {
        vf = 1;
    }
    return vf;
}



char * retirerAccent(char * str) {
    char accent[] = "�����������������������������������������������������";
    char sansAccent[] = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc";
    int i= 0, j = 0;
    while (str[i]) {
        j = 0;
        while(accent[j]) {
            if(str[i] == accent[j]){
                str[i] = sansAccent[j];
            }
            j ++;
        }
        i ++;
    }
    return str;
}

