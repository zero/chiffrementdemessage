Rouillon Ambre
Vignolles Martin
Groupe B 

Le but de cette application est de pouvoir coder ou décoder un message avec le code de César en entrant le messsage et sa clé.


int verificationAlpa (char str[]) :
Cette fonction permet de savoir si tous les caractères entrer sont des lettres de l'alphabet. Si c'est le cas la fonction renverra 1 sinon elle renverra 0. Cela correspond à vrai ou faux. char str[] est la chaine de caractère qui est entré par l'utilisateur. Aucun cas d'erreur trouver.


char* DecCesar(char * str, int cle) :
Permet de décoder le message. On place en entrer le mot à décoder (char * str) et la clé (un entier). la fonction revoit le message ainsi décodé. Cas d'erreur : ne reviens pas à z si on entre "a" ou "A".


char* cesar(char * str, int cle) :
Permet de coder le message. On place en entrer le mot à coder (char * str) et la clé (un entier). la fonction revoit le message codé. Aucun cas d'erreur trouver.


char* retirerAccent(char * str) : 
La fonction permet de retirer les accents de la chaine de caractères. Elle prend en entrée la chaine de caractère à modifier et la revoit une fois les accents retirés. Aucun cas d'erreur trouver.


Lien Git : https://framagit.org/zero/chiffrementdemessage.git
